<?php

/*
 * Plugin Name: Tauzero weather widget
 * Version: 0.1
 * Plugin URI: http://jlkonsultab.se/
 * Description: Widget for showing weather data from tauzero.se
 * Author: Johan Linderoth
 * Author URI: http://jlkonsultab.se
 */

require_once(dirname(__FILE__).'/TZWeatherWidget.php');

class TZWeather {

    private $plugin_name;
    // constructor
    function TZWeather() {

        if(function_exists('is_admin')) {
            if(is_admin()) {
                $this->plugin_name = plugin_basename(__FILE__);
                register_activation_hook($this->plugin_name,array($this, 'activatePlugin'));
                register_deactivation_hook($this->plugin_name,array($this, 'deactivatePlugin'));
            }
        }
        add_action('wp_head', 'TZWeather::insertIntoHeadSection');
    }

    function activatePlugin() {
    }

    function deactivatePlugin() {
    }

    public static function insertIntoHeadSection() {
        ?>
        <!-- TauzeroWeather plugin -->
        <link rel="stylesheet" type="text/css" media="all" href="<?php echo TZWeather::pluginBaseUrl();?>/tauzero.min.css" />
        <!-- TauzeroWeather plugin -->
        <?php
    }

    public static function pluginBaseUrl() {
        return plugins_url(basename(dirname(__FILE__)));
    }

}

// startup!
$w7321fc7328f057b0c15dafb00eff2eb3 = new TZWeather();

?>
