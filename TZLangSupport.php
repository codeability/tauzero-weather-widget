<?php
class TZLangSupport {

    public static function langSupportIsQtranslate() {
        return function_exists('qtrans_getLanguage');
    }


    public static function langSupportIsWPML() {
        return function_exists('icl_get_languages');
    }


    public static function langSupportEnabled() {
        return TZLangSupport::langSupportIsWPML() || TZLangSupport::langSupportIsQtranslate();
    }


    public static function getLangCodes() {
        global $sitepress;

        $result = array();
        if(TZLangSupport::langSupportIsQtranslate()) {
            $result = qtrans_getSortedLanguages();
        } else if(TZLangSupport::langSupportIsWPML()) {

            foreach($sitepress->get_active_languages() as $language) {
                $result[] = $language['code'];
            }
        }
        return $result;
    }


    public static function getCurrentLangCode() {

        if(TZLangSupport::langSupportIsQtranslate()) {

            return qtrans_getLanguage();

        } else if(TZLangSupport::langSupportIsWPML()) {

            return ICL_LANGUAGE_CODE;

        } else {

            return null;

        }
    }


    public static function getLanguageNameByCode($code)  {
        global $q_config, $sitepress;

        if(TZLangSupport::langSupportIsQtranslate()) {

            return $q_config['language_name'][$code];

        } else if(TZLangSupport::langSupportIsWPML()) {

            foreach($sitepress->get_active_languages() as $language) {
                if($language['code'] == $code)
                    return $language['native_name'];
            }
            return '';

        } else {

            return '';

        }
    }

    public static function getCurrentLanguageNameByCode()  {
        return TZLangSupport::getLanguageNameByCode(TZLangSupport::getCurrentLangCode());
    }
}
?>