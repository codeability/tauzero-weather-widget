<?php

require_once(dirname(__FILE__).'/TZLangSupport.php');

class TZWidgetBase extends WP_Widget {

    function TZWidgetBase($className, $description) {
        parent::WP_Widget(false, $className, array('description' => $description, 'width' => 430 ));
    }

    function formField($instance, $name, $label, $widget=null) {
        if($widget==null) $widget=$this;
        $value = esc_attr($instance[$name]);
        ?>
        <p>
            <label for="<?php echo $widget->get_field_id($name); ?>"><?php echo $label; ?></label>
            <input class="widefat" id="<?php echo $widget->get_field_id($name); ?>" name="<?php echo $widget->get_field_name($name); ?>" type="text" value="<?php echo $value; ?>" />
        </p>
    <?php
    }


    function formTextArea($instance, $name, $label, $widget=null) {
        if($widget==null) $widget=$this;
        $value = esc_attr($instance[$name]);
        ?>
        <p>
            <label for="<?php echo $widget->get_field_id($name); ?>"><?php echo $label; ?></label>
            <textarea class="widefat" rows="16" cols="20" id="<?php echo $widget->get_field_id($name); ?>" name="<?php echo $widget->get_field_name($name); ?>" rows="8"><?php echo $value; ?></textarea>
        </p>
    <?php
    }


    /**
     * create input fields or textarea fields, one per active language
     *
     * @param $instance the widget instance
     * @param $fieldname field name (language code will be appended)
     * @param $fieldLabel field label (language name will be appended)
     * @param $type tyoe of field 'textarea', anything else renders a simple input text field
     * @param $header a header label for all fields
     * @param null $widget
     *
     */
    function formFieldsPerLanguage($instance, $fieldname, $fieldLabel, $type, $header, $widget=null) {
        if($widget==null) $widget=$this;
        if($header) {
            echo "<h3>$header</h3>";
        }
        if(TZLangSupport::langSupportEnabled()) {
            foreach(TZLangSupport::getLangCodes() as $language) {
                switch($type) {
                    case 'textarea':
                        $this->formTextArea($instance, $fieldname.'_'.$language, $fieldLabel.' in <strong>'.TZLangSupport::getLanguageNameByCode($language).'</strong>', $widget);
                        break;
                    default:
                        $this->formField($instance, $fieldname.'_'.$language, $fieldLabel.' in <strong>'.TZLangSupport::getLanguageNameByCode($language).'</strong>', $widget);
                }

            }
        } else {
            switch($type) {
                case 'textarea':
                    $this->formTextArea($instance, $fieldname, $fieldLabel, $widget);
                    break;
                default:
                    $this->formField($instance, $fieldname, $fieldLabel, $widget);
            }
        }

    }



    function formSelect($instance, $name, $label, $values, $widget=null) {
        if($widget==null) $widget=$this;
        $selectedKey = esc_attr($instance[$name]);
        ?>
        <p>
            <label for="<?php echo $widget->get_field_id($name); ?>"><?php echo $label; ?></label>
            <select id="<?php echo $widget->get_field_id($name); ?>" name="<?php echo $widget->get_field_name($name); ?>">
                <option value="">Select format... <?php echo $selectedKey?></option>
                <?php
                foreach(array_keys($values) as $key) {
                    if($key == $selectedKey) {
                        echo "<option value=\"$key\" selected>$values[$key]</option>";
                    } else {
                        echo "<option value=\"$key\">$values[$key]</option>";
                    }
                }
                ?>

            </select>
        </p>
    <?php
    }


    /**
     * Example usage:
     *
    $this->formRadio($instance,
    'titleLevel',   // field name
    'Field label',  // field label in widget GUI
    array('h2' => ' H2',  // radio options (option value => option label)
    'h3' => ' H3',
    'h4' => ' H4',
    'h5' => ' H5'));
     */

    function formRadio($instance, $name, $label, $values, $widget=null) {
        if($widget==null) $widget=$this;
        $selectedKey = esc_attr($instance[$name]);
        ?>
        <p>
            <strong><label for="<?php echo $widget->get_field_id($name); ?>"><?php echo $label; ?></label></strong>
            <?php
            foreach(array_keys($values) as $key) {
                if($key == $selectedKey) {
                    echo "<br/><input type=\"radio\" value=\"$key\" name=\"".$widget->get_field_name($name)."\" checked>$values[$key]</option>";
                } else {
                    echo "<br/><input type=\"radio\" value=\"$key\" name=\"".$widget->get_field_name($name)."\" >$values[$key]</option>";
                }
            }
            ?>
        </p>
    <?php
    }

    function formCheckbox($instance, $name, $label, $widget=null) {
        if($widget==null) $widget=$this;
        $checked = esc_attr($instance[$name]);
        ?>
        <p>
            <strong><label for="<?php echo $widget->get_field_id($name); ?>"><?php echo $label; ?></label></strong>
            <?php
            if($checked == 1) {
                echo "<br/><input type=\"checkbox\" value=\"1\" name=\"".$widget->get_field_name($name)."\" checked>";
            } else {
                echo "<br/><input type=\"checkbox\" value=\"1\" name=\"".$widget->get_field_name($name)."\" >";
            }
            ?>
        </p>
    <?php
    }

    function getLanguageField($instance, $name) {
        $value = '';
        if(TZLangSupport::langSupportEnabled()) {
            $value = $instance[$name.'_'.TZLangSupport::getCurrentLangCode()];
        } else {
            $value = $instance[$name];
        }
        return $value;
    }

    function updateField($name, $new_instance, $old_instance) {
        $instance = $old_instance;
        $instance[$name] = strip_tags($new_instance[$name]);
        return $instance;
    }

    function updateLanguageField($name, $new_instance, $old_instance) {
        $instance = $old_instance;

        if(TZLangSupport::langSupportEnabled()) {
            foreach(TZLangSupport::getLangCodes() as $language) {
                $instance[$name.'_'.$language] = $new_instance[$name.'_'.$language];
            }
        } else {
            $instance[$name] = $new_instance[$name];
        }

        return $instance;
    }
}

?>