<?php

require_once(dirname(__FILE__) . '/TZWidgetBase.php');
require_once(dirname(__FILE__) . '/TZLangSupport.php');

class TZWeatherWidget extends TZWidgetBase
{

    function TZWeatherWidget()
    {
        parent::__construct('Tauzero_Weather_Widget', 'Tauzero Weather Widget');
    }


    function form($instance)
    {
        $this->formRadio($instance, 'presentation', 'Presentation',
            array('table' => ' Table',
                'row' => ' In a row'
            ));
    }


    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;

        if (!$new_instance['presentation']) {
            $new_instance['presentation'] = 'row';
        }
        $instance['presentation'] = $new_instance['presentation'];
        return $instance;
    }


    function widget($args, $instance)
    {
        $before_widget = '';
        $after_widget = '';
        extract($args);

        $i18n = array(
            'en' => array(
                'weather' => 'Weather',
                'temp_out' => 'Temp',
                'hum_out' => 'Humidity',
                'wind_speed' => 'Wind speed',
                'wind_speed_avg' => 'Avg wind speed',
                'wind_speed_max' => 'Max wind speed',
                'wind_direction' => 'Wind direction',
                'wind_chill' => 'Wind cooling',
                'dewpnt' => 'Dew point',
                'pressure' => 'Barometric pressure',
                'snow' => 'Snow depth',
                'rain_24h' => 'Rain today',
                'rain_total' => 'Rain last month',
                'Falling' => 'falling',
                'Rising' => 'rising'
            ),
            'sv' => array(
                'weather' => 'Väder',
                'temp_out' => 'Temp',
                'hum_out' => 'Luftfuktighet',
                'wind_speed' => 'Vind',
                'wind_speed_avg' => 'Medelvind',
                'wind_speed_max' => 'Maxvind',
                'wind_direction' => 'Vindriktning',
                'wind_chill' => 'Kyleffekt',
                'dewpnt' => 'Daggpunkt',
                'pressure' => 'Lufttryck',
                'snow' => 'Snödjup',
                'rain_24h' => 'Regn idag',
                'rain_total' => 'Regn senaste månad',
                'Falling' => 'fallande',
                'Rising' => 'stigande'
            ),
            'de' => array(
                'weather' => 'Wetter',
                'temp_out' => 'Temperatur',
                'hum_out' => 'Luftfeuchtigkeit',
                'wind_speed' => 'Wind',
                'wind_speed_avg' => 'Durchschnittliche Wind',
                'wind_speed_max' => 'Maximale Wind',
                'wind_direction' => 'Windrichtung',
                'wind_chill' => 'Kühlung',
                'dewpnt' => 'Taupunkt',
                'pressure' => 'Luftdruck',
                'snow' => 'Schneehöhe',
                'rain_24h' => 'Regen heute',
                'rain_total' => 'Regen letzten Monat',
                'Falling' => 'fallendem',
                'Rising' => 'steigendem'
            )
        );

        echo $before_widget;
        $url = 'http://www.tauzero.se/weatherjson.php';

        $jsonData = file_get_contents($url);

        $data = json_decode($jsonData, true);

        if ($data['status'] && $data['status'] == 'ok') {

            $date = $data['date'];
            $time = $data['time'];
            $temp_out = $data['temp_out'];
            $hum_out = $data['hum_out'];
            $wind_speed = $data['wind_speed'];
            $wind_speed_avg = $data['wind_speed_avg'];
            $wind_speed_max = $data['wind_speed_max'];
            $wind_direction = $data['wind_direction'];
            $wind_angle0 = $data['wind_angle0'];
//            $wind_angle1 = $data['wind_angle1'];
//            $wind_angle2 = $data['wind_angle2'];
//            $wind_angle3 = $data['wind_angle3'];
//            $wind_angle4 = $data['wind_angle4'];
//            $wind_angle5 = $data['wind_angle5'];
            $wind_chill = $data['wind_chill'];
            $dewpnt = $data['dewpnt'];
            $pressure = $data['pressure'];
            $tendency = $data['tendency'];
            $snow = $data['snow'];
            $rain_24h = $data['rain_24h'];
            $rain_total = $data['rain_total'];

            $lang = $i18n[TZLangSupport::getCurrentLangCode()];
            $directions = array('S', 'W', 'E', 'N');
            switch(TZLangSupport::getCurrentLangCode()) {
                case 'sv':
                    $wind_direction = str_replace($directions, array('syd', 'väst', 'ost', 'nord'), $wind_direction);
                    break;
                case 'en':
                    $wind_direction = str_replace($directions, array('south-', 'west-', 'east-', 'north'), $wind_direction);
                    break;
                case 'de':
                    $wind_direction = str_replace('E', 'Ö', $wind_direction);
                    break;
            }
            $wind_direction = preg_replace("/-$/", '', $wind_direction);

            if($instance['presentation'] == 'table') {

                echo "
                    <table>
                      <tr>
                        <td><strong>" . $lang['weather'] . "</strong></td><td>$date $time</td>
                      </tr>
                      <tr>
                        <td><strong>" . $lang['temp_out'] . "</strong></td><td>$temp_out &deg;C</td>
                      </tr>
                      <tr>
                          <td><strong>" . $lang['hum_out'] . "</td><td>$hum_out %</td>
                      </tr>
                      <tr>
                          <td><strong>" . $lang['wind_speed'] . "</td><td>$wind_speed m/s</td>
                      </tr>
                      <tr>
                          <td><strong>" . $lang['wind_speed_avg'] . "</td><td>$wind_speed_avg m/s</td>
                      </tr>
                      <tr>
                          <td><strong>" . $lang['wind_speed_max'] . "</td><td>$wind_speed_max m/s</td>
                      </tr>
                      <tr>
                          <td><strong>" . $lang['wind_direction'] . "</td><td>$wind_direction ($wind_angle0 &deg;)</td>
                      </tr>
                      <tr>
                          <td><strong>" . $lang['wind_chill'] . "</td><td>$wind_chill &deg;C</td>
                      </tr>
                      <tr>
                          <td><strong>" . $lang['dewpnt'] . "</td><td>$dewpnt &deg;C</td>
                      </tr>
                      <tr>
                          <td><strong>" . $lang['pressure'] . "</td><td>$pressure hPa (".$lang[$tendency].")</td>
                      </tr>
                      <tr>
                          <td><strong>" . $lang['snow'] . "</td><td>$snow</td>
                      </tr>
                      <tr>
                          <td><strong>" . $lang['rain_24h'] . "</td><td>$rain_24h</td>
                      </tr>
                      <tr>
                          <td><strong>" . $lang['rain_total'] . "</td><td>$rain_total</td>
                      </tr>

                    </table>"
                ;
            } else {
                echo "
                    <div class='tz-weather-summary'>
                      <div class='tz-non-breaking-block'>
                        <strong>" . $lang['weather'] . "</strong>: $date $time
                      </div>
                      <div class='tz-non-breaking-block'>
                        <strong>" . $lang['temp_out'] . "</strong>: $temp_out &deg;C
                      </div>
                      <div class='tz-non-breaking-block'>
                          <strong>" . $lang['hum_out'] . "</strong>: $hum_out %
                      </div>
                      <div class='tz-non-breaking-block'>
                          <strong>" . $lang['wind_speed'] . "</strong>: $wind_speed m/s
                      </div>
                      <div class='tz-non-breaking-block'>
                          <strong>" . $lang['wind_speed_avg'] . "</strong>: $wind_speed_avg m/s
                      </div>
                      <div class='tz-non-breaking-block'>
                          <strong>" . $lang['wind_speed_max'] . "</strong>: $wind_speed_max m/s
                      </div>
                      <div class='tz-non-breaking-block'>
                          <strong>" . $lang['wind_direction'] . "</strong>: $wind_direction ($wind_angle0 &deg;)
                      </div>
                      <div class='tz-non-breaking-block'>
                          <strong>" . $lang['wind_chill'] . "</strong>: $wind_chill &deg;C
                      </div>
                      <div class='tz-non-breaking-block'>
                          <strong>" . $lang['dewpnt'] . "</strong>: $dewpnt &deg;C
                      </div>
                      <div class='tz-non-breaking-block'>
                          <strong>" . $lang['pressure'] . "</strong>: $pressure hPa (".$lang[$tendency].") <!-- $tendency -->
                      </div>
                      <div class='tz-non-breaking-block'>
                          <strong>" . $lang['snow'] . "</strong>: $snow
                      </div>
                      <div class='tz-non-breaking-block'>
                          <strong>" . $lang['rain_24h'] . "</strong>: $rain_24h
                      </div>
                      <div class='tz-non-breaking-block'>
                          <strong>" . $lang['rain_total'] . "</strong>: $rain_total
                      </div>
                  </div>"
                ;
            }
        }

        echo $after_widget;
    }


} // class Tauzero_Weather_Widget

// register widget
add_action('widgets_init', create_function('', 'return register_widget("TZWeatherWidget");'));

?>
