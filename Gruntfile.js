module.exports = function(grunt) {

    grunt.initConfig({
        // JAVASCRIPT
        jshint: {
            files: ['Gruntfile.js'],
            options: {
                globals: {
                    jQuery: true
                }
            }
        },

        // uglify: {
        //     js: {
        //         files: {
        //             'tauzero.min.js': ['tauzero.js']
        //         }
        //     }
        // },

        // CSS
        less: {
            compile: {
                files: {
                    "tauzero.css": "tauzero.less"
                }
            }
        },

        cssmin: {
            options: {},
            target: {
                files: {
                    'tauzero.min.css': ['tauzero.css']
                }
            }
        },

        // WATCH
        watch: {
            files: ['<%= jshint.files %>', '*.less'],
            tasks: ['jshint', 'less', 'cssmin']
        }

    });

    grunt.loadNpmTasks("grunt-contrib-cssmin");
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-less');
//    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('css', ['less', 'cssmin']);
    grunt.registerTask('default', ['jshint', 'css']);

};